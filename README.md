# SimpleLogin
 - A minimal login system for paper servers running in offline mode
 - The plugin does not require any extra-world for login
 - The plugin will send any player who attempts to log in a fake black box and wait for them to enter their password.
 - The password hash is saved in an SQLite database.
 - All not-logged-in players are invisible to others and others are invisible to them

## Commands
 - /register ... for new players
 - /login ... to log in
 - /passwd ... to change the password

## Permissions
None. If you want to change the password of any user as an admin, just delete it from the plugins/simplelogin/auth.db file. You can also set another password, however, for that you'd need to hash it first.

Note: You can allow players to use the same password all across your network by symlinking auth.db. However, the plugin is not designed for large server networks.
