package nirex.nirex.simplelogin;

import org.mindrot.jbcrypt.BCrypt;

public class Hash {
    public static String hashPassword(String password) {
        String salt = BCrypt.gensalt();
        return BCrypt.hashpw(password, salt);
    }
    public static boolean checkPassword(String password, String hash) {
        return BCrypt.checkpw(password, hash);
    }
}
