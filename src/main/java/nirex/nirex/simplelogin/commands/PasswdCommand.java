package nirex.nirex.simplelogin.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import nirex.nirex.simplelogin.Hash;
import nirex.nirex.simplelogin.SimpleLogin;

public class PasswdCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String args[]) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Only playeth'rs may useth this hest!");
            return true;
        }
        Player player = (Player) sender;
        if(args.length != 2) {
            player.sendMessage(ChatColor.YELLOW + "Usage:" + ChatColor.GRAY + 
                    " /passwd <old_password> <new_password>");
            return true;
        }

        String password = SimpleLogin.getPassword(player);

        if(Hash.checkPassword(args[0], password)) {
            SimpleLogin.setPassword(player, args[1]);
            player.sendMessage(ChatColor.GREEN + "Grant you mercy f'r telling me thy secret w'rd!");
        } else {
            player.sendMessage(ChatColor.RED + "Thee has't ang'r'd the theatre-provid'r! Bid me the real secret w'rd!");
            return true;
        }
        return false;   
    }
}
