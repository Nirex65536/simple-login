package nirex.nirex.simplelogin.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import net.md_5.bungee.api.ChatColor;
import nirex.nirex.simplelogin.Hash;
import nirex.nirex.simplelogin.SimpleLogin;

public class LoginCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String Label, String args[]) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Only playeth'rs may useth this hest!");
            return true;
        }
        Player player = (Player) sender;
        if(player.hasMetadata("loggedin") && player.getMetadata("loggedin").get(0).asBoolean()) {
            player.sendMessage(ChatColor.GRAY + "Thee has't already taken thy lodging in this theatre");
            return true;
        }
        if(args.length != 1) {
            player.sendMessage(ChatColor.YELLOW + "Usage:" + ChatColor.GRAY + 
                    " /login <password>");
            return true;
        }

        String password = SimpleLogin.getPassword(player);

        if(Hash.checkPassword(args[0], password)) {
            player.setMetadata("loggedin", new FixedMetadataValue(SimpleLogin.getInstance(), true));
            player.sendMessage(ChatColor.GREEN + "Thou art welcometh to ent'r the theatre!");
        } else {
            player.kickPlayer(ChatColor.RED + "Thee has't ang'r'd the theatre-provid'r! Thou shall die!");
            return true;
        }

        int px = (int) player.getLocation().getX();
        int py = (int) player.getLocation().getY();
        int pz = (int) player.getLocation().getZ();
        for(int x = px - 6; x < px + 6; x++) {
            for(int y = py - 6; y < py + 6; y++) {
                for(int z = pz - 6; z < pz + 6; z++) {
                    Location loc = new Location(player.getWorld(), x, y, z);
                    player.sendBlockChange(loc, loc.getBlock().getBlockData());
                }
            }
        }
        return false;   
    }
}
