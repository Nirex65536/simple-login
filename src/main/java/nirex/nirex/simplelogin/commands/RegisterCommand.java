package nirex.nirex.simplelogin.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import net.md_5.bungee.api.ChatColor;
import nirex.nirex.simplelogin.SimpleLogin;

public class RegisterCommand implements CommandExecutor {

    @Override 
    public boolean onCommand(CommandSender sender, Command command, String label, String args[]) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Only playeth'rs may useth this hest!");
            return true;
        }
        Player player = (Player) sender;
        if(SimpleLogin.isRegistered(player)) {
            player.sendMessage(ChatColor.GRAY + "The theatre-provider doest already knoweth thy nameth!");
            return true;
        }
        if(args.length != 2 || !args[0].equals(args[1])) {
            player.sendMessage(ChatColor.YELLOW + "Usage:" + ChatColor.GRAY + 
                    " /register <password> <repeat_password>");
            return true;
        }
        if(args[0].length() > 32) {
            player.sendMessage(ChatColor.GRAY + "Thy secret w'rd wilt not beest longeth'r than 32 charact'rs");
            return true;
        }

        SimpleLogin.setPassword(player, args[0]);
        player.setMetadata("loggedin", new FixedMetadataValue(SimpleLogin.getInstance(), true));

        player.sendMessage(ChatColor.GREEN + "Thanketh thee f'r joining our theatre! Enjoyeth the showeth!");

        int px = (int) player.getLocation().getX();
        int py = (int) player.getLocation().getY();
        int pz = (int) player.getLocation().getZ();
        for(int x = px - 6; x < px + 6; x++) {
            for(int y = py - 6; y < py + 6; y++) {
                for(int z = pz - 6; z < pz + 6; z++) {
                    Location loc = new Location(player.getWorld(), x, y, z);
                    player.sendBlockChange(loc, loc.getBlock().getBlockData());
                }
            }
        }
        return false;
    }
}
