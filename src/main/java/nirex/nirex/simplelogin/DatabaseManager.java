package nirex.nirex.simplelogin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseManager {
    private Connection connection;

    public void connect(String databse) throws SQLException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection("jdbc:sqlite:" + databse);
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
