package nirex.nirex.simplelogin;

import java.util.logging.Level;
import java.io.File;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import nirex.nirex.simplelogin.commands.LoginCommand;
import nirex.nirex.simplelogin.commands.PasswdCommand;
import nirex.nirex.simplelogin.commands.RegisterCommand;

public class SimpleLogin extends JavaPlugin {

    private static SimpleLogin instance;
    private DatabaseManager auth;

    public static SimpleLogin getInstance() {
        return instance;
    }

    public DatabaseManager getAuth() {
        return auth;
    }

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(new JoinListener(), this);
        Bukkit.getPluginManager().registerEvents(new ActionListener(), this);
        getCommand("register").setExecutor(new RegisterCommand());
        getCommand("login").setExecutor(new LoginCommand());
        getCommand("passwd").setExecutor(new PasswdCommand());

        HidePlayer.runPlayerHider();

        File pluginFolder = new File(getDataFolder().getParent(), "simplelogin");
        if (!pluginFolder.exists()) {
            pluginFolder.mkdir();
        }
        auth = new DatabaseManager();
        try {
            auth.connect("plugins/simplelogin/auth.db");
            Connection connection = auth.getConnection();
            Statement statement = connection.createStatement(); 

            statement.executeUpdate("CREATE TABLE IF NOT EXISTS logins (uuid TEXT, shadow TEXT)");

            statement.close();
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            Bukkit.getLogger().log(Level.WARNING, 
                    "[SimpleLogin] Failed to initialize database, disabling plugin ...");
            Bukkit.getServer().getPluginManager().disablePlugin(this);
        }
    }

    @Override
    public void onDisable() {

    }

    public static boolean isRegistered(Player player) {
        try {
            instance.getAuth().connect("plugins/simplelogin/auth.db");

            Connection connection = SimpleLogin.getInstance().getAuth().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM logins WHERE uuid = ?");

            statement.setString(1, player.getUniqueId().toString());

            ResultSet resultSet = statement.executeQuery();
            connection.close();
            return resultSet.next();
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static void setPassword(Player player, String password) {
        try {
            instance.getAuth().connect("plugins/simplelogin/auth.db");
            String hash = Hash.hashPassword(password);
            Connection connection = instance.getAuth().getConnection();
            PreparedStatement delete = connection.prepareStatement(
                    "DELETE FROM logins WHERE uuid = ?");
            delete.setString(1, player.getUniqueId().toString());

            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO logins (uuid, shadow) VALUES (?, ?)");
            statement.setString(1, player.getUniqueId().toString());
            statement.setString(2, hash);

            delete.executeUpdate();
            statement.executeUpdate();

            delete.close();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String getPassword(Player player) {
        try {
            instance.getAuth().connect("plugins/simplelogin/auth.db");
            Connection connection = instance.getAuth().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT shadow FROM logins WHERE uuid = ?");
            statement.setString(1, player.getUniqueId().toString());
            ResultSet result = statement.executeQuery();
            if(result.next()) {
                String password = result.getString("shadow");
                statement.close();
                connection.close();
                return password;
            }
            statement.close();
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
