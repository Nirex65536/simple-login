package nirex.nirex.simplelogin;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class HidePlayer {
    private static void hideAll() {
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            if (player.hasMetadata("loggedin")) {
                if (!player.getMetadata("loggedin").get(0).asBoolean()) {
                    for (Player otherPlayer : Bukkit.getServer().getOnlinePlayers()) {
                        otherPlayer.hidePlayer(SimpleLogin.getInstance(), player);
                        player.hidePlayer(SimpleLogin.getInstance(), otherPlayer);
                    }
                } else {
                    for (Player otherPlayer : Bukkit.getServer().getOnlinePlayers()) {
                        otherPlayer.showPlayer(SimpleLogin.getInstance(), player);
                        player.showPlayer(SimpleLogin.getInstance(), otherPlayer);
                    }
                }

            }
        }
    }

    private static void sendFakeData() {
        for(Player player : Bukkit.getServer().getOnlinePlayers()) {
            if(player.hasMetadata("loggedin") && !player.getMetadata("loggedin").get(0).asBoolean()){
                int px = (int) player.getLocation().getX();
                int py = (int) player.getLocation().getY();
                int pz = (int) player.getLocation().getZ();
                for(int x = px - 5; x < px + 5; x++) {
                    for(int y = py - 5; y < py + 5; y++) {
                        for(int z = pz - 5; z < pz + 5; z++) {
                            Location loc = new Location(player.getWorld(), x, y, z);
                            BlockData data = Material.BLACK_CONCRETE.createBlockData();
                            player.sendBlockChange(loc, data);
                        }
                    }
                }
                for(int x = px - 1; x < px + 1; x++) {
                    for(int y = py - 1; y < py + 1; y++) {
                        for(int z = pz - 1; z < pz + 1; z++) {
                            Location loc = new Location(player.getWorld(), x, y, z);
                            BlockData data = Material.AIR.createBlockData();
                            player.sendBlockChange(loc, data);
                        }
                    }
                }
            }
        }
    }

    public static void runPlayerHider() {
        new BukkitRunnable() {
            @Override
            public void run() {
                hideAll();
                sendFakeData();
            }
        }.runTaskTimer(SimpleLogin.getInstance(), 0L, 5L);
    }
}
