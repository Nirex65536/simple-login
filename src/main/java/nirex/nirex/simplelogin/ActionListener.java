package nirex.nirex.simplelogin;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupArrowEvent;
import org.bukkit.metadata.MetadataValue;

import com.destroystokyo.paper.event.player.PlayerPickupExperienceEvent;

import io.papermc.paper.event.player.AsyncChatEvent;
import net.md_5.bungee.api.ChatColor;

public class ActionListener implements Listener {

    private void handleEvent(Player player, Cancellable event) {
        for (MetadataValue metadata : player.getMetadata("loggedin")) {
            if (metadata.getOwningPlugin().equals(SimpleLogin.getInstance()) && !metadata.asBoolean()) {
                event.setCancelled(true);
                if(SimpleLogin.isRegistered(player)) {
                    player.sendMessage(ChatColor.YELLOW + "Please log in using /login <password>");
                } else {
                    player.sendMessage(ChatColor.YELLOW + "Please use /register <password> <repeat_password> to sign up!");
                }
            }
        }
    }
    private void handleEventSilent(Player player, Cancellable event) {
        for (MetadataValue metadata : player.getMetadata("loggedin")) {
            if (metadata.getOwningPlugin().equals(SimpleLogin.getInstance()) && !metadata.asBoolean()) {
                event.setCancelled(true);
            }
        }
    }


    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        handleEvent(e.getPlayer(), e);
    }
    @EventHandler
    public void onPlaceBlock(BlockPlaceEvent e) {
        handleEvent(e.getPlayer(), e);
    }
    @EventHandler
    public void onBreakBlock(BlockBreakEvent e) {
        handleEvent(e.getPlayer(), e);
    }
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        handleEvent(e.getPlayer(), e);
    }
    @EventHandler
    public void onChat(AsyncChatEvent e) {
        handleEvent(e.getPlayer(), e);
    }
    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        handleEvent(e.getPlayer(), e);
    }
    @EventHandler
    public void onPickupArrow(PlayerPickupArrowEvent e) {
        handleEvent(e.getPlayer(), e);
    }
    @EventHandler
    public void onExp(PlayerPickupExperienceEvent e) {
        handleEvent(e.getPlayer(), e);
    }
    @EventHandler
    public void onPickup(EntityPickupItemEvent e) {
        if(!(e.getEntity() instanceof Player)) return;
        Player player = (Player) e.getEntity();
        handleEvent(player, e);
    }
    @EventHandler
    public void onInvClick(InventoryClickEvent e) {
        if(!(e.getWhoClicked() instanceof Player)) return;
        Player player = (Player) e.getWhoClicked();
        handleEvent(player, e);
    }
    @EventHandler
    public void onHit(EntityDamageByEntityEvent e) {
        if(!(e.getDamager() instanceof Player)) return;
        Player player = (Player) e.getDamager();
        handleEvent(player, e);
    }
    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if(!(e.getEntity() instanceof Player)) return;
        Player player = (Player) e.getEntity();
        handleEventSilent(player, e);
    }
    @EventHandler
    public void onTarget(EntityTargetLivingEntityEvent e) {
        if(!(e.getTarget() instanceof Player)) return;
        Player player = (Player) e.getTarget();
        handleEventSilent(player, e);
    }
    @EventHandler
    public void onInvokeCommand(PlayerCommandPreprocessEvent e) {
        int index = e.getMessage().indexOf(" ");
        String command = index == -1 ?
            e.getMessage().substring(1) :
            e.getMessage().substring(1, e.getMessage().indexOf(" "));
        Player player = e.getPlayer();
        if(!command.equals("login") && !command.equals("register")) {
            handleEvent(e.getPlayer(), e);
        } 
    }
}
