package nirex.nirex.simplelogin;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.metadata.FixedMetadataValue;

import net.md_5.bungee.api.ChatColor;

public class JoinListener implements Listener {
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        player.setMetadata("loggedin", new FixedMetadataValue(SimpleLogin.getInstance(), false));
        
        if(SimpleLogin.isRegistered(player)) {
            player.sendMessage(ChatColor.GREEN + "Welcometh backeth! Prithee useth /login <passw'rd> to joineth again");
        } else {
            player.sendMessage(ChatColor.GREEN + "Welcometh to the tbsrv03 theatre! Prithee useth /regist'r <passw'rd> <repeat_passw'rd> to join the theatre!");
        }
    }
}
